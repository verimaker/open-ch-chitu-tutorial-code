/**
  ******************************************************************
  * @file    main.c
  * @author  xy,仁面
  * @version V1.0
  * @date    2022-1-13
  * @brief   使用按键 Wake_Up 控制 LED1 亮灭。
  ******************************************************************
  * @attention
  * verimake 用于ch32v307例程
  ******************************************************************
  */
#include "debug.h"//包含ch32v307的头文件，c标准单元库和delay()函数
/********************************************************************
* 函 数 名       : GPIO_INIT
* 函数功能    : GPIO 初始化
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void GPIO_INIT(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure={0};

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE,ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
}
/********************************************************************
* 函 数 名       : EXTI_INT_INIT
* 函数功能    : 外部中断初始化
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void EXTI_INT_INIT(void)
{
   GPIO_InitTypeDef  GPIO_InitStructure={0};
   EXTI_InitTypeDef EXTI_InitStructure={0};
   NVIC_InitTypeDef NVIC_InitStructure={0};

   RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOA,ENABLE);

   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
   GPIO_Init(GPIOA, &GPIO_InitStructure);

   /* GPIOA ----> EXTI_Line0 */
   GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource0);
   EXTI_InitStructure.EXTI_Line=EXTI_Line0;
   EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
   EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising; //按下为高电平，用上升沿
   EXTI_InitStructure.EXTI_LineCmd = ENABLE;
   EXTI_Init(&EXTI_InitStructure);

   NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn ;
   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; //抢占优先级
   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;        //子优先级
   NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&NVIC_InitStructure);
}

/********************************************************************
* 函 数 名      :  main
* 函数功能   : 主函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
int main(void)
{
    EXTI_INT_INIT();//外部中断初始化
    GPIO_INIT();//GPIO 初始化
    while(1);//死循环
}

/********************************************************************
* 函 数 名      : EXTI0_IRQHandler
* 函数功能   : 中断服务函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
void EXTI0_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
volatile uint16_t LED_Status = 0; //中断里使用的变量加 volatile 成为全局变量。
void EXTI0_IRQHandler(void)
{
    EXTI_ClearFlag(EXTI_Line0);  //清理中断标志位
    LED_Status = !LED_Status ;   //将LED的状态取反
    GPIO_WriteBit(GPIOE, GPIO_Pin_11, LED_Status);//配置 PE11 即 LED1 状态
}
