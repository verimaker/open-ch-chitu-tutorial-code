/**
  ******************************************************************
  * @file    main.c
  * @author  xy,Benue
  * @version V1.0
  * @date    2022-1-15
  * @brief   lcd 背光亮度变化。
  ******************************************************************
  * @attention
  * VeriMake 用于CH32V307例程
  ******************************************************************
  */
#include "debug.h"// 包含 CH32V307 的头文件，C 标准单元库和delay()函数
/********************************************************************
* 函 数 名       : TIM6_Init
* 函数功能    : 初始化 定时器 TIM6
* 输    入          : arr：计数值，psc 预分频系数
* 输    出          : 无
********************************************************************/
void TIM6_Init( u16 arr, u16 psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

    RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM6, ENABLE );

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInit( TIM6, &TIM_TimeBaseInitStructure);

    TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
    TIM_ARRPreloadConfig( TIM6, ENABLE );
    TIM_Cmd( TIM6, ENABLE );
}

/*******************************************************************************
*    函 数 名                       : TIM1_Init
*    函数功能                    : Initializes TIM1
*    输    入                          : arr:                 周期值
*                     psc:                 预分频值
*                     ccp:                 脉冲宽度
*    输    出                         : None
*******************************************************************************/
void TIM1_Init( u16 arr, u16 psc, u16 ccp )
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_TIM1, ENABLE );

    /* TIM1_CH2N */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( GPIOB, &GPIO_InitStructure );

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit( TIM1, &TIM_TimeBaseInitStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
    TIM_OCInitStructure.TIM_Pulse = ccp;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;     //高电平有效
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;  //停止时为低电平
    TIM_OC2Init( TIM1, &TIM_OCInitStructure );

    TIM_CtrlPWMOutputs(TIM1, ENABLE );
    TIM_OC1PreloadConfig( TIM1, TIM_OCPreload_Disable );
    TIM_ARRPreloadConfig( TIM1, ENABLE );
    TIM_Cmd( TIM1, ENABLE );
}

/********************************************************************
* 函 数 名       : Interrupt_Init
* 函数功能    : 初始化定时器中断
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void Interrupt_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure={0};
    NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn ;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; //抢占优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;        //子优先级
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/********************************************************************
*    函 数 名     ：  main
* 函数功能   : 主函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
int main(void)
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断控制器的优先级分组为 占优先级 2位 ,优先级2位。
    TIM6_Init( 5000-1, 14400-1 ); // 初始化定时器，让 LED 1 秒闪烁一次，我们需要让定时器 0.5 秒溢出，要计数 `144M * 0.5 = 72M` 个时钟周期,而定时器只有16位，这是不够的。需要用到预分频器，设分频系数为 14400,可以得到 10KHz 的定时器时钟，这样设置计数值 5000 就可以做到 0.5 ms 定时。
    TIM1_Init(5000-1,0,2500);//周期值5000-1，预分频系数0，脉冲宽度2500
    Interrupt_Init();//初始化定时器中断
    while(1);        // 死循环
}

/********************************************************************
*    函 数 名      : TIM6_IRQHandler
* 函数功能   : 中断服务程序的函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
void TIM6_IRQHandler(void)   __attribute__((interrupt("WCH-Interrupt-fast")));
volatile uint16_t brightness = 0; // 中断里使用的变量加 volatile 可当成全局变量
void TIM6_IRQHandler(void)
{
    TIM_ClearFlag(TIM6, TIM_FLAG_Update); //清标志位
    brightness += 500;//每0.5s亮度增加
    brightness %= 5000;//亮度达到5000以上时，将亮度重置为1
    TIM_SetCompare2(TIM1, brightness);
}
