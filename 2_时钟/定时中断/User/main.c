/**
  ******************************************************************
  * @file    main.c
  * @author  xy,Benue
  * @version V1.0
  * @date    2022-1-15
  * @brief   控制 LED1 1秒闪烁一次。
  ******************************************************************
  * @attention
  * VeriMake 用于CH32V307例程
  ******************************************************************
  */
#include "debug.h"// 包含 CH32V307 的头文件，C 标准单元库和delay()函数
/********************************************************************
* 函 数 名       : GPIO_INIT
* 函数功能    : 初始化 GPIO
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void GPIO_INIT(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure={0};

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE,ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
}

/********************************************************************
* 函 数 名       : TIM6_Init
* 函数功能    : 初始化 定时器 TIM6
* 输    入          : arr：计数值，psc 预分频系数
* 输    出          : 无
********************************************************************/
void TIM6_Init( u16 arr, u16 psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

    RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM6, ENABLE );

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInit( TIM6, &TIM_TimeBaseInitStructure);

    TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
    TIM_ARRPreloadConfig( TIM6, ENABLE );
    TIM_Cmd( TIM6, ENABLE );
}

/********************************************************************
* 函 数 名       : Interrupt_Init
* 函数功能    : 初始化定时器中断
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void Interrupt_Init(void)
{
   NVIC_InitTypeDef NVIC_InitStructure={0};
   NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn ;
   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; //抢占优先级
   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;        //子优先级
   NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&NVIC_InitStructure);
}

/********************************************************************
*    函 数 名     ：  main
* 函数功能   : 主函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
int main(void)
{
    GPIO_INIT();     // 初始化 GPIO
    TIM6_Init( 5000-1, 14400-1 ); // 初始化定时器，让 LED 1 秒闪烁一次，我们需要让定时器 0.5 秒溢出，要计数 `144M * 0.5 = 72M` 个时钟周期,而定时器只有16位，这是不够的。需要用到预分频器，设分频系数为 14400,可以得到 10KHz 的定时器时钟，这样设置计数值 5000 就可以做到 0.5 ms 定时。
    Interrupt_Init();//初始化定时器中断
    while(1);        // 死循环
}

/********************************************************************
*    函 数 名      : TIM6_IRQHandler
* 函数功能   : 中断服务程序的函数
* 输    入         : 无
* 输    出         : 无
*********************************************************************/
void TIM6_IRQHandler(void)   __attribute__((interrupt("WCH-Interrupt-fast")));
volatile uint16_t LED_Status = 0; // 中断里使用的变量加 volatile 可当成全局变量
void TIM6_IRQHandler(void)
{
    TIM_ClearFlag(TIM6, TIM_FLAG_Update);//清除标志位
    LED_Status = !LED_Status ;  // 将 LED 状态值取反
    GPIO_WriteBit(GPIOE, GPIO_Pin_11, LED_Status); // 配置 PE11 (即 LED1) 状态
}
