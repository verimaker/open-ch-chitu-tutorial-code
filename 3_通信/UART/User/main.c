/**
  ******************************************************************
  * @file    main.c
  * @author  xy,Benue
  * @version V1.0
  * @date    2022-1-19
  * @brief   UART 收发例程（轮询）：使用 USART2 收发数据
  ******************************************************************
  * @attention
  * VeriMake 用于CH32V307例程
  ******************************************************************
  */
#include "debug.h"// 包含 CH32V307 的头文件，C 标准单元库和delay()函数

/********************************************************************
* 函 数 名       : USART2_Init
* 函数功能    : 初始化 usart2
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void USART2_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    //开启时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    /* USART2 TX-->PA2  RX-->PA3 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;           //RX，输入上拉
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = 115200;                    // 波特率
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;     // 数据位 8
    USART_InitStructure.USART_StopBits = USART_StopBits_1;          // 停止位 1
    USART_InitStructure.USART_Parity = USART_Parity_No;             // 无校验
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // 无硬件流控
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; //使能 RX 和 TX

    USART_Init(USART2, &USART_InitStructure);

    USART_Cmd(USART2, ENABLE);                                        //开启UART
}

/********************************************************************
* 函 数 名       : main
* 函数功能    : 主函数
* 输    入          : 无
* 输    出          : 无
********************************************************************/
int main(void)
{
    USART2_Init();           /* USART INIT */
    int i = 0;
    char str[]="Loop back from USART2.\r\n";     //发送一条提示语
    while(str[i]){
        while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);    //等待 上次发送 结束
        USART_SendData(USART2, str[i]);                                 //发送数据
        i++;
    }
    Delay_Ms(500);
    int recv;
    while(1){
        //串口回环，把接收到的数据原样发出
        while(USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);   //等待接收数据
        recv = USART_ReceiveData(USART2);                               //读取接收到的数据
        while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);    //等待 上次发送 结束
        USART_SendData(USART2, recv);                                   //发送数据
    }
}

