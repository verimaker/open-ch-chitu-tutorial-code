/**
  ******************************************************************
  * @file    main.c
  * @author  xy,Benue
  * @version V1.0
  * @date    2022-2-10
  * @brief   ADC使用DMA进行采样
  ******************************************************************
  * @attention
  * VeriMake 用于CH32V307例程
  ******************************************************************
  */
#include "debug.h"// 包含 CH32V307 的头文件，C 标准单元库和delay()函数

/* Global Variable */
u16 TxBuf[1024];
s16 Calibrattion_Val = 0;


/********************************************************************
* 函 数 名       : ADC_Function_Init
* 函数功能    : 初始化ADC
* 输    入          : 无
* 输    出          : 无
********************************************************************/
void ADC_Function_Init(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE );
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE );
    RCC_ADCCLKConfig(RCC_PCLK2_Div8);               //初始化ADC时钟，设置时钟为PCLK2的8分频，最大时钟为14MHz

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);          //配置PA1口为AD输入口

    ADC_DeInit(ADC1);
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;  //设置AD模式为单独模式，只使用ADC1
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;       //禁用多通道模式，启用单通道模式
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;  //启动连续转换模式
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; //不启用外部触发源，启动软件触发
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;  //数据右对齐
    ADC_InitStructure.ADC_NbrOfChannel = 1;     //要转换通道数量
    ADC_Init(ADC1, &ADC_InitStructure);

    ADC_DMACmd(ADC1, ENABLE); //启动DMA
    ADC_Cmd(ADC1, ENABLE);          //使能ADC

    ADC_BufferCmd(ADC1, DISABLE);   //disable buffer
    ADC_ResetCalibration(ADC1);
    while(ADC_GetResetCalibrationStatus(ADC1));
    ADC_StartCalibration(ADC1);
    while(ADC_GetCalibrationStatus(ADC1));
    Calibrattion_Val = Get_CalibrationValue(ADC1);

    ADC_BufferCmd(ADC1, ENABLE);   //enable buffer
}

/********************************************************************
* 函 数 名       : DMA_Tx_Init
* 函数功能    : DMA 初始化
* 输    入          : DMA_CHx:
*          x can be 1 to 7.
*          ppadr:  数据源地址
*          memadr: 数据目的地址
*          bufsize: 传输的数据大小
* 输    出          : 无
********************************************************************/
void DMA_Tx_Init( DMA_Channel_TypeDef* DMA_CHx, u32 ppadr, u32 memadr, u16 bufsize)
{
    DMA_InitTypeDef DMA_InitStructure;

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_DMA1, ENABLE );

    DMA_DeInit(DMA_CHx);
    DMA_InitStructure.DMA_PeripheralBaseAddr = ppadr;       //源地址
    DMA_InitStructure.DMA_MemoryBaseAddr = memadr;          //目的地址
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;      //DMA方向从外设到MEM
    DMA_InitStructure.DMA_BufferSize = bufsize;             //传输大小
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;    //外设地址自增关
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;             //MEM地址自增开
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; //外设传输数据为半字，16位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;         //MEM传输数据为半字，16位
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;           //普通模式
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh; //优先级设置为高
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;            //关闭MEM到MEM传输
    DMA_Init( DMA_CHx, &DMA_InitStructure );


}

/*******************************************************************************
* Function Name  : main
* Description    : Main program.
* Input          : None
* Return         : None
*******************************************************************************/
int main(void)
{
    u16 i;

    Delay_Init();
    USART_Printf_Init(115200);
    printf("SystemClk:%d\r\n",SystemCoreClock);

    ADC_Function_Init();
    printf("CalibrattionValue:%d\n", Calibrattion_Val);
  
    DMA_Tx_Init( DMA1_Channel1, (u32)&ADC1->RDATAR, (u32)TxBuf, 1024 );  //设置DMA 通道DMA1的通道1，源地址是AD采样数据寄存器，目的地址是RAM，数据量1024个
    DMA_Cmd( DMA1_Channel1, ENABLE );

    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_1Cycles5 );
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);     //软件触发启动AD转换
    Delay_Ms(50);
    ADC_SoftwareStartConvCmd(ADC1, DISABLE);

    for(i=0; i<1024; i++)
    {
        printf( "%04d\r\n", TxBuf[i] );
        Delay_Ms(10);
    }

    while(1);
}







