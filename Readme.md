# openCH 赤菟教程代码

本仓库存放着 VeriMake 赤菟教程的代码。

## 相应的教程

- ***快速上手指南：<https://verimake.com/d/37-opench-ch32v307-risc-v>***

## 其它资料

- ***另一个例程代码仓库：<https://gitee.com/verimaker/opench-ch32v307>***
- [***openCH 赤菟开发板原理图***](./doc/SCH_openCH_CH32V307_Board.pdf)
- [***CH32V307 介绍页面 (WCH)***](http://www.wch.cn/products/CH32V307.html)
- [***CH32V307 沁恒官方例程（用于赤菟开发板时需修改）***](http://www.wch.cn/downloads/CH32V307EVT_ZIP.html)  
- [***CH32V307 芯片手册 (WCH)***](http://www.wch.cn/downloads/CH32V20x_30xDS0_PDF.html)
- [***CH32V307 参考手册 (WCH)***](http://www.wch.cn/downloads/CH32FV2x_V3xRM_PDF.html)

*如果您在使用 openCH 赤菟 开发板 的过程中遇到问题，欢迎在 **[VeriMake 论坛赤菟版块](https://verimake.com/t/Chitu)** 参与讨论。如果发现文档中存在问题，欢迎在 **[这个贴子](https://verimake.com/d/37-opench-ch32v307-risc-v)** 下留言讨论。*